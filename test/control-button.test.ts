import { html, fixture, expect } from '@open-wc/testing';

import { ControlButton } from '../src/ControlButton.js';
import '../control-button.js';

describe('ControlButton', () => {
  it('has a default title "Hey there" and counter 5', async () => {
    const el = await fixture<ControlButton>(html`<control-button></control-button>`);

    expect(el.title).to.equal('Hey there');
    // expect(el.counter).to.equal(5);
  });

  it('increases the counter on button click', async () => {
    const el = await fixture<ControlButton>(html`<control-button></control-button>`);
    el.shadowRoot!.querySelector('button')!.click();

    // expect(el.counter).to.equal(6);
  });

  it('can override the title via attribute', async () => {
    const el = await fixture<ControlButton>(html`<control-button title="attribute title"></control-button>`);

    expect(el.title).to.equal('attribute title');
  });

  it('passes the a11y audit', async () => {
    const el = await fixture<ControlButton>(html`<control-button></control-button>`);

    await expect(el).shadowDom.to.be.accessible();
  });
});
