import {SetControllerMixin} from './SetControllerMixin.js';
import {ControlButtonBase} from './ControlButtonBase.js';

export class SetControlButton extends SetControllerMixin(ControlButtonBase) {
}
