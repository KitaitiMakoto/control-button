import { ControlButtonBase } from './ControlButtonBase.js';
import { AddControllerMixin } from './AddControllerMixin.js';

export class AddControlButton extends AddControllerMixin(ControlButtonBase) {
}
