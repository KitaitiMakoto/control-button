import {ControlButtonBase} from './ControlButtonBase.js';

export interface OpenableCandidate extends HTMLElement {
  open?: boolean;
}

type Openable = Required<OpenableCandidate>;

function isOpenable(control: OpenableCandidate, property: string): control is Openable {
  return typeof (control as any)[property] === 'boolean';
}

export function ToggleControllerMixin(Base: typeof ControlButtonBase) {
  return class extends Base {
    get property(): string {
      return super.property || 'open';
    }

    _onClick() {
      this._toggleTarget();
    }

    _toggleTarget() {
      for (const control of this.controls) {
        if (this._controlIsOpenable(control)) {
          (control as any)[this.property] = !(control as any)[this.property];
        }
      }
    }

    _controlIsOpenable(control: OpenableCandidate): control is Openable {
      return isOpenable(control, this.property);
    }
  };
}
