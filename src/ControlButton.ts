import {ControlButtonBase} from './ControlButtonBase.js';
import {ToggleControllerMixin} from './ToggleControllerMixin.js';
import {SetControllerMixin} from './SetControllerMixin.js';
import {AddControllerMixin} from './AddControllerMixin.js';

export class ControlButton extends AddControllerMixin(SetControllerMixin(ToggleControllerMixin(ControlButtonBase))) {
  get type(): string {
    return this.#detectType() ?? '';
  }

  set type(newValue: string) {
    this.setAttribute('type', newValue);
  }

  _onClick() {
    // FIXME: type of `this`
    switch (this.type) {
      case 'set':
        (this as any)._setValue();
        break;
      case 'add':
        (this as any)._addValue();
        break;
      case 'toggle':
        (this as any)._toggleTarget();
        break;
      default:
        // noop
    }
  }

  #detectType(): string | null {
    if (this.hasAttribute('type')) {
      return this.getAttribute('type');
    }
    if (Array.from(this.controls).every(control => (this as any)._controlIsOpenable(control))) {
      return 'toggle';
    }
    return null;
  }
}
