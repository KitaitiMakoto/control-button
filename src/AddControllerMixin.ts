import {ControlButtonBase} from './ControlButtonBase.js';
import {PROPERTY_CONVERTER} from './PropertyAttributeMixin.js';

export function AddControllerMixin(Base: typeof ControlButtonBase) {
  return class extends Base {
    _onClick() {
      this._addValue();
    }

    _addValue() {
      const convert = PROPERTY_CONVERTER[this.propertyType] ?? PROPERTY_CONVERTER.string;
      for (const control of this.controls) {
        const current = (control as any)[this.property];
        (control as any)[this.property] = convert(current) + this.value;
      }
    }
  };
}
