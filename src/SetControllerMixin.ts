import {ControlButtonBase} from "./ControlButtonBase.js";

export function SetControllerMixin(Base: typeof ControlButtonBase) {
  return class extends Base  {
    _onClick() {
      this._setValue();
    }

    _setValue() {
      for (const control of this.controls) {
        (control as any)[this.property] = this.value;
      }
    }
  };
}
