import {ControlButtonBase} from './ControlButtonBase.js';
import {ToggleControllerMixin} from './ToggleControllerMixin.js';

export class ToggleControlButton extends ToggleControllerMixin(ControlButtonBase) {
}
