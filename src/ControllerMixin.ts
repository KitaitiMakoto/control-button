// @ts-ignore
import TokenList from '../../node_modules/tokenlist/src/tokenlist.js';

export function ControllerMixin(Base: typeof HTMLElement) {
  return class extends Base {
    static get observedAttributes(): string[] {
      return ['aria-controls'];
    }

    readonly ariaControls: DOMTokenList = new TokenList(
      () => this.getAttribute('aria-controls'),
      (value: string) => this.setAttribute('aria-controls', value)
    );

    get controls(): NodeListOf<HTMLElement> {
      if (this.ariaControls.length === 0) {
        return this.querySelectorAll('nothing'); // FIXME: selector
      }
      let selector = '';
      for (const id of this.ariaControls) {
        const sel = `#${id}`;
        if (selector) {
          selector += `,#{sel}`;
        } else {
          selector = sel;
        }
      }
      if (! selector) {
        return this.querySelectorAll('nothing'); // FIXME: selector
      }
      return document.querySelectorAll(selector);
    }

    constructor() {
      super();
      this.addEventListener('click', this._onClick)
    }

    /* eslint-disable class-methods-use-this */
    /* eslint-disable @typescript-eslint/no-unused-vars */
    _onClick(event: MouseEvent) {
      // placeholder for subclasses
    }
  }
}
