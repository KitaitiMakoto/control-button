import {ControllerMixin} from './ControllerMixin.js';

type PropertyType = 'string' | 'number';

export const PROPERTY_CONVERTER = {
  string: (value: any) => value,
  number: (value: any) => Number(value)
};

export function PropertyAttributeMixin(Base: ReturnType<typeof ControllerMixin>) {
  return class extends Base {
    static get observedAttributes(): string[] {
      return super.observedAttributes.concat(['property', 'property-type', 'value']);
    }

    get property(): string {
      return this.getAttribute('property') ?? '';
    }

    set property(newValue: string) {
      this.setAttribute('property', newValue);
    }

    get propertyType(): PropertyType {
      return (this.getAttribute('property-type') ?? 'string') as PropertyType;
    }

    set propertyType(newValue: PropertyType) {
      this.setAttribute('property-type', newValue);
    }

    get value(): any {
      const value = this.getAttribute('value');
      const convert = PROPERTY_CONVERTER[this.propertyType] ?? PROPERTY_CONVERTER.string;
      return convert(value);
    }

    set value(newValue: any) {
      this.setAttribute('value', newValue.toString());
    }
  }
};
