import {ControllerMixin} from './ControllerMixin.js';
import {PropertyAttributeMixin} from './PropertyAttributeMixin.js';

export class ControlButtonBase extends PropertyAttributeMixin(ControllerMixin(HTMLElement)) {
  static get observedAttributes(): string[] {
    return super.observedAttributes.concat(['disabled']);
  }

  constructor() {
    super();
    this.attachShadow({mode: 'open'}).innerHTML = `
      <style>
        :host {
          display: inline-block;
          box-sizing: border-box;
          padding: 0.25em;
          border: solid 1px rgb(126, 126, 126);
          border-radius: 0.3em;
          background-color: rgb(240, 240, 240);
        }

        :host(:hover) {
          background-color: rgb(216, 216, 216);
        }

        :host(:active) {
          background-color: rgba(196, 196, 196);
        }

        :host([disabled]) {
          border-color: rgb(196, 196, 196);
          color: rgb(126, 126, 126);
          background-color: rgb(240, 240, 240);
        }

        button {
          font: inherit;
          padding: 0;
          border: none;
          letter-spacing: inherit;
          background-color: inherit;
          color: inherit;
        }

        button:disabled {
          border-color: inherit;
          color: inherit;
          background-color: inherit;
        }
      </style>
      <button type="button"><slot></slot></button>
    `;
  }

  connectedCallback() {
    if (! this.hasAttribute('role')) {
      this.setAttribute('role', 'button');
    }
  }

  attributeChangedCallback(name: string, oldValue: string, newValue: string) {
    switch (name) {
      case 'disabled':
        this.#button.disabled = newValue !== null;
        break;
      default:
    }
  }

  get disabled(): boolean {
    return this.hasAttribute('disabled');
  }

  set disabled(newValue: boolean) {
    this.toggleAttribute('disabled', newValue)
  }

  get #button(): HTMLButtonElement {
    return this?.shadowRoot?.querySelector('button') as HTMLButtonElement;
  }
}
